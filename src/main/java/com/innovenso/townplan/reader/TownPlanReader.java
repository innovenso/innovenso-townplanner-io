package com.innovenso.townplan.reader;

import com.innovenso.townplan.domain.TownPlanImpl;

public interface TownPlanReader {
	TownPlanImpl read();
}
