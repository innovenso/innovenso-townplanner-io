package com.innovenso.townplan.reader.exceptions;

public class TownPlanReaderException extends RuntimeException {
	public TownPlanReaderException() {
	}

	public TownPlanReaderException(String message) {
		super(message);
	}

	public TownPlanReaderException(String message, Throwable cause) {
		super(message, cause);
	}

	public TownPlanReaderException(Throwable cause) {
		super(cause);
	}
}
