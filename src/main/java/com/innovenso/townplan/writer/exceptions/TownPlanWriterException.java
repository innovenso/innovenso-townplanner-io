package com.innovenso.townplan.writer.exceptions;

public class TownPlanWriterException extends RuntimeException {
	public TownPlanWriterException() {
	}

	public TownPlanWriterException(String message) {
		super(message);
	}

	public TownPlanWriterException(String message, Throwable cause) {
		super(message, cause);
	}

	public TownPlanWriterException(Throwable cause) {
		super(cause);
	}
}
