package com.innovenso.townplan.writer;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

import java.io.File;

public interface TownPlanWriter {
	void write(TownPlan townPlan);

	void write(TownPlan townPlan, String conceptKey);

	File getTargetBaseDirectory();

	void setTargetBaseDirectory(@NonNull final File targetBaseDirectory);
}
