package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.domain.KeyPointInTimeImpl;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.time.LocalDate;
import java.util.Comparator;

@Log4j2
public class ConceptWriterModel<T extends Concept> {
	private final T concept;
	private final KeyPointInTime pointInTime;
	private final TownPlan townPlan;

	public ConceptWriterModel(@NonNull final T concept, @NonNull final KeyPointInTime pointInTime,
			@NonNull TownPlan townPlan) {
		this.concept = concept;
		this.pointInTime = pointInTime;
		this.townPlan = townPlan;
	}

	public T getConcept() {
		return concept;
	}

	public TownPlan getTownPlan() {
		return townPlan;
	}

	public String getKey() {
		return concept.getKey();
	}

	public String getType() {
		return concept.getType();
	}

	public String getTitle() {
		return concept.getTitle();
	}

	public String getDescription() {
		return concept.getDescription();
	}

	public File getOutputFile(String provider, String format, String view) {
		return concept.getOutputFile(provider, format, view);
	}

	public String getCategory() {
		return concept.getCategory();
	}

	public KeyPointInTime getPointInTime() {
		return pointInTime;
	}

	public boolean isActive(Concept concept) {
		return concept.isActive(this.pointInTime);
	}

	public boolean isRendered(Concept concept) {
		final boolean rendered = isActive(concept) || isPhasingOut(concept) || isUnknownLifecycle(concept)
				|| isRemovedSinceLastKeyPointInTime(concept);
		log.info("is concept {} rendered at point in time {}?: {}", concept.getKey(), this.pointInTime, rendered);
		return rendered;
	}

	public boolean isRelationshipRendered(Relationship relationship) {
		return isRendered(relationship.getSource()) && isRendered(relationship.getTarget());
	}

	public boolean isPlanned(Concept concept) {
		return concept.isPlanned(this.pointInTime);
	}

	public boolean isPhasingOut(Concept concept) {
		return concept.isPhasingOut(this.pointInTime);
	}

	public boolean isDecommissioned(Concept concept) {
		return concept.isDecommissioned(this.pointInTime);
	}

	public boolean isUnknownLifecycle(Concept concept) {
		return concept.isUnknown(this.pointInTime);
	}

	public boolean isAddedSinceLastKeyPointInTime(Concept concept) {
		return concept.isActive(this.pointInTime) && !concept.isActive(getPreviousKeyPointInTime());
	}

	public boolean isRemovedSinceLastKeyPointInTime(Concept concept) {
		return concept.isDecommissioned(this.pointInTime) && !concept.isDecommissioned(getPreviousKeyPointInTime());
	}

	protected KeyPointInTime getPreviousKeyPointInTime() {
		return townPlan.getKeyPointsInTime().stream()
				.filter(pointInTime -> pointInTime.getDate().isBefore(this.pointInTime.getDate()))
				.max(Comparator.comparing(KeyPointInTime::getDate))
				.orElse(KeyPointInTimeImpl.builder().date(LocalDate.MIN).build());
	}
}
