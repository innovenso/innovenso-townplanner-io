package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.writer.exceptions.TownPlanWriterException;
import lombok.NonNull;

import java.util.Optional;
import java.util.Set;

public class ElementWriterModel<T extends Element> extends ConceptWriterModel<T> {

	public ElementWriterModel(@NonNull final T element, @NonNull final KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public Set<Element> getDirectDependencies() {
		return getConcept().getDirectDependencies();
	}

	public Set<Element> getDirectDependencies(@NonNull final String elementClassName) {
		return getConcept().getDirectDependencies(getElementClass(elementClassName));
	}

	public Set<Element> getDirectIncomingDependencies() {
		return getConcept().getDirectIncomingDependencies();
	}

	public Set<Element> getDirectOutgoingDependencies() {
		return getConcept().getDirectOutgoingDependencies();
	}

	public Set<Element> getChildDependencies() {
		return getConcept().getChildDependencies();
	}

	public Set<Element> getChildDependencies(@NonNull final String elementClassName) {
		return getConcept().getChildDependencies(getElementClass(elementClassName));
	}

	public Set<Relationship> getIncomingRelationships() {
		return getConcept().getIncomingRelationships();
	}

	public Set<Relationship> getOutgoingRelationships() {
		return getConcept().getOutgoingRelationships();
	}

	public Set<Element> getChildren() {
		return getConcept().getChildren();
	}

	public Optional<Element> getParent() {
		return getConcept().getParent();
	}

	public boolean hasChildren() {
		return getConcept().hasChildren();
	}

	protected Class<? extends Element> getElementClass(@NonNull final String className) throws TownPlanWriterException {
		try {
			return Class.forName(className).asSubclass(Element.class);
		} catch (ClassNotFoundException e) {
			throw new TownPlanWriterException(e);
		}
	}
}
