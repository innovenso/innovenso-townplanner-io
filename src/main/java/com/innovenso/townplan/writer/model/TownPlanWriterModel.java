package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.api.value.view.FlowView;
import lombok.NonNull;

import java.util.Set;
import java.util.stream.Collectors;

public record TownPlanWriterModel(TownPlan townPlan) {

	public Set<Enterprise> getEnterprises() {
		return townPlan.getElements(Enterprise.class);
	}

	public Set<BusinessCapability> getCapabilities() {
		return townPlan.getElements(BusinessCapability.class);
	}

	public Set<ArchitectureBuildingBlock> getBuildingBlocks() {
		return townPlan.getElements(ArchitectureBuildingBlock.class);
	}

	public Set<ItSystem> getSystems() {
		return townPlan.getElements(ItSystem.class);
	}

	public Set<BusinessActor> getPeople() {
		return townPlan.getElements(BusinessActor.class).stream().filter(BusinessActor::isPerson)
				.collect(Collectors.toSet());
	}

	public Set<BusinessActor> getSquads() {
		return townPlan.getElements(BusinessActor.class).stream().filter(BusinessActor::isSquad)
				.collect(Collectors.toSet());
	}

	public Set<ItProject> getProjects() {
		return townPlan.getElements(ItProject.class);
	}

	public Set<ItProjectMilestone> getMilestones() {
		return townPlan.getElements(ItProjectMilestone.class);
	}

	public Set<ItContainer> getContainers() {
		return townPlan.getElements(ItContainer.class);
	}

	public Set<ItSystemIntegration> getIntegrations() {
		return townPlan.getElements(ItSystemIntegration.class);
	}

	public Set<FlowView> getViews() {
		return townPlan.getFlowViews();
	}

	public Set<Technology> getTechnologies() {
		return townPlan.getElements(Technology.class);
	}

	public Set<Decision> getDecisions() {
		return townPlan.getElements(Decision.class);
	}
}
