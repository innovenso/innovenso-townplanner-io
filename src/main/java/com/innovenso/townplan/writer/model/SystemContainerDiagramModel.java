package com.innovenso.townplan.writer.model;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItSystem;
import lombok.NonNull;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SystemContainerDiagramModel extends ElementWriterModel<ItSystem> {
	public SystemContainerDiagramModel(@NonNull ItSystem element, @NonNull final KeyPointInTime keyPointInTime,
			@NonNull final TownPlan townPlan) {
		super(element, keyPointInTime, townPlan);
	}

	public boolean hasContainers() {
		return !getContainers().isEmpty();
	}

	public ItSystem getCentralSystem() {
		return getConcept();
	}

	public Set<? extends Element> getSystems() {
		Set<? extends Element> systems = Sets.union(getCentralSystem().getDirectDependencies(ItSystem.class),
				getCentralSystem().getChildDependencies(ItSystem.class));
		return systems.stream().filter(this::isRendered)
				.filter(system -> !system.getKey().equals(getCentralSystem().getKey())).collect(Collectors.toSet());
	}

	public Set<ItContainer> getContainers() {
		return getCentralSystem().getContainers().stream().filter(this::isRendered).collect(Collectors.toSet());
	}

	public Set<? extends Element> getActors() {
		if (!hasContainers())
			return getCentralSystem().getDirectDependencies(BusinessActor.class).stream().filter(this::isRendered)
					.collect(Collectors.toSet());
		else
			return getCentralSystem().getChildDependencies(BusinessActor.class).stream().filter(this::isRendered)
					.collect(Collectors.toSet());
	}

	public Set<Relationship> getRelationships() {
		if (!hasContainers())
			return Sets.union(getCentralSystem().getRelationships(ItSystem.class).stream()
					.filter(relationship -> isRendered(relationship, getCentralSystem())).collect(Collectors.toSet()),
					getCentralSystem().getRelationships(BusinessActor.class).stream()
							.filter(relationship -> isRendered(relationship, getCentralSystem()))
							.collect(Collectors.toSet()));
		else {
			final Set<Relationship> internalContainerRelationships = new HashSet<>();
			getContainers().forEach(container -> {
				container.getRelationships().stream().filter(relationship -> isRendered(relationship, container))
						.forEach(relationship -> {
							if (shouldIncludeContainerRelationship(container, relationship))
								internalContainerRelationships.add(relationship);
						});
			});
			return internalContainerRelationships;
		}
	}

	boolean isRendered(Relationship relationship, Element source) {
		return isRendered(relationship.getOther(source));
	}

	public Set<Relationship> getFlows() {
		return getRelationships().stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.FLOW))
				.collect(Collectors.toSet());
	}

	private boolean shouldIncludeContainerRelationship(@NonNull final ItContainer container,
			@NonNull final Relationship relationship) {
		return isInternalContainerRelationship(container, relationship)
				|| isExternalSystemOrActorRelationship(container, relationship);
	}

	private boolean isInternalContainerRelationship(@NonNull final ItContainer container,
			@NonNull final Relationship relationship) {
		if (relationship.isCircular())
			return true;
		Element other = relationship.getOther(container);
		return other.getParent().isPresent() && other.getParent().get().getKey().equals(getCentralSystem().getKey());
	}

	private boolean isExternalSystemOrActorRelationship(@NonNull final ItContainer container,
			@NonNull final Relationship relationship) {
		Element other = relationship.getOther(container);
		return other instanceof ItSystem || other instanceof BusinessActor;
	}
}
