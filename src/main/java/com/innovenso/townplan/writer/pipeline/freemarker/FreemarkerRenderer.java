package com.innovenso.townplan.writer.pipeline.freemarker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.*;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Log4j2
public class FreemarkerRenderer {
	private final Configuration configuration;

	public FreemarkerRenderer(@NonNull final String baseTemplatePath) {
		this.configuration = new Configuration(Configuration.VERSION_2_3_30);
		this.configuration.setClassForTemplateLoading(this.getClass(), "/" + baseTemplatePath);
	}

	public Optional<File> write(@NonNull final Map<String, Object> model, @NonNull final String templateName) {
		try {
			final Template template = getTemplate(templateName);
			final File outputFile = File.createTempFile(templateName, UUID.randomUUID().toString());
			final Writer outputFileWriter = new OutputStreamWriter(new FileOutputStream(outputFile));
			try {
				template.process(model, outputFileWriter);
			} catch (TemplateException e) {
				log.error("Could not process template {}", template);
				return Optional.empty();
			} finally {
				outputFileWriter.close();
			}
			log.info("wrote file {}", outputFile.getAbsolutePath());
			return Optional.of(outputFile);
		} catch (IOException e) {
			log.error(e);
			return Optional.empty();
		}
	}

	private Template getTemplate(String templateName) throws IOException {
		try {
			return configuration.getTemplate(templateName);
		} catch (TemplateNotFoundException tnfe) {
			log.info("template for element not found, loading empty template");
			return configuration.getTemplate("empty.ftl");
		}
	}
}
