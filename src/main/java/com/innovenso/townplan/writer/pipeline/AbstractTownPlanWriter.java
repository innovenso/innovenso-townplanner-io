package com.innovenso.townplan.writer.pipeline;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.command.AddContentDistributionAspectCommand;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.domain.TownPlanImpl;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.TownPlanWriter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Log4j2
public abstract class AbstractTownPlanWriter implements TownPlanWriter {
	private final AssetRepository assetRepository;
	private final ContentOutputType contentOutputType;

	protected AbstractTownPlanWriter(@NonNull final AssetRepository assetRepository,
			@NonNull final ContentOutputType contentOutputType) {
		this.assetRepository = assetRepository;
		this.contentOutputType = contentOutputType;
	}

	@Override
	public void write(@NonNull final TownPlan townPlan) {
		final List<File> renderedFiles = new ArrayList<>();
		writeFiles(townPlan, renderedFiles);
		writeZipFileAndCreateContentDistribution(townPlan, renderedFiles);
	}

	public void write(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		writeFiles(townPlan, renderedFiles);
		postWriteFiles(townPlan, renderedFiles);
	}

	public void write(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles,
			@NonNull String conceptKey) {
		writeFiles(townPlan, renderedFiles, conceptKey);
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, @NonNull String conceptKey) {
		final List<File> renderedFiles = new ArrayList<>();
		writeFiles(townPlan, renderedFiles, conceptKey);
	}

	public void postWriteFiles(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		log.info("not post processing written files");
	}

	protected abstract void writeFiles(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles);

	protected abstract void writeFiles(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles,
			@NonNull String conceptKey);

	protected AssetRepository getAssetRepository() {
		return assetRepository;
	}

	protected void recordCdnUrl(@NonNull final TownPlan townPlan, @NonNull String cdnUrl, @NonNull String elementKey,
			@NonNull final String title) {
		Optional.of(townPlan).filter(TownPlanImpl.class::isInstance).map(TownPlanImpl.class::cast)
				.ifPresent(townPlanImpl -> townPlanImpl.execute(AddContentDistributionAspectCommand.builder()
						.url(cdnUrl).modelComponentKey(elementKey).type(contentOutputType).title(title).build()));
	}

	private ZipEntry getZipEntry(File file) {
		log.info("adding file to zip: {}", file.getAbsolutePath());
		Path pathAbsolute = file.toPath().toAbsolutePath();
		Path pathBase = getTargetBaseDirectory().toPath().toAbsolutePath();
		Path pathRelative = pathBase.relativize(pathAbsolute);
		return new ZipEntry(pathRelative.toString());
	}

	private void writeZipFileAndCreateContentDistribution(@NonNull final TownPlan townPlan,
			@NonNull final List<File> renderedFiles) {
		File outputFile = new File(getTargetBaseDirectory(), townPlan.getId().getEntityId().getValue() + "-"
				+ this.contentOutputType.getLabel().toLowerCase() + ".zip");
		try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
				ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {
			for (File renderedConcept : renderedFiles) {
				try (FileInputStream fis = new FileInputStream(renderedConcept)) {
					zipOutputStream.putNextEntry(getZipEntry(renderedConcept));
					byte[] bytes = new byte[1024];
					int length;
					while ((length = fis.read(bytes)) >= 0) {
						zipOutputStream.write(bytes, 0, length);
					}
				}
			}
		} catch (IOException e) {
			log.error(e);
		}
		assetRepository.write(outputFile, outputFile.getName());
		createContentDistribution(townPlan, outputFile.getName(),
				"Zip file containing " + contentOutputType.getLabel() + " exports");
	}

	private void createContentDistribution(@NonNull final TownPlan townPlan, @NonNull final String outputFileName,
			@NonNull final String title) {
		if (townPlan instanceof TownPlanImpl) {
			TownPlanImpl tpi = (TownPlanImpl) townPlan;
			assetRepository.getCdnUrl(outputFileName).ifPresent(cdnUrl -> {
				tpi.getElements(Enterprise.class)
						.forEach(enterprise -> tpi
								.execute(AddContentDistributionAspectCommand.builder().type(ContentOutputType.ZIP)
										.modelComponentKey(enterprise.getKey()).url(cdnUrl).title(title).build()));
			});
		}
	}
}
