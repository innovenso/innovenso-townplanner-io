package com.innovenso.townplan.writer.pipeline;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Log4j2
public class ClassPathResourceFileProvider {

	public void writeFile(@NonNull final File outputFile, @NonNull final String classPathResourceLocation) {
		try {
			log.info("template resource {}", classPathResourceLocation);
			final InputStream stream = getClass().getResourceAsStream(classPathResourceLocation);
			if (stream != null) {
				log.info("creating file {}", outputFile.getAbsolutePath());
				FileUtils.copyInputStreamToFile(stream, outputFile);
				log.info("File created");
			}
		} catch (IOException e) {
			log.error("Can not fetch class path resource file {}", classPathResourceLocation);
		}
	}
}
