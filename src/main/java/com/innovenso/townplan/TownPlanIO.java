package com.innovenso.townplan;

import com.innovenso.townplan.reader.TownPlanReader;
import com.innovenso.townplan.writer.TownPlanWriter;

import java.util.Optional;

public interface TownPlanIO {
	Optional<TownPlanReader> getReader();
	Optional<TownPlanWriter> getWriter();

	default boolean canRead() {
		return getReader().isPresent();
	}
	default boolean canWrite() {
		return getWriter().isPresent();
	}

	String getKey();
	String getTitle();
	String getDescription();
}
